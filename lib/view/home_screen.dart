import 'package:flutter/material.dart';
import 'package:project1/constants.dart';
import 'package:project1/view/file_information_screen.dart';
import 'package:project1/view/login_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: screenColor,
      body: SafeArea(
        child: ListView(
          shrinkWrap: true,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  padding: EdgeInsets.all(5.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10.0),
                    child: Container(
                      color: primaryColor,
                      child: IconButton(
                        onPressed:() {
                          Navigator.of(context)
                            .push(MaterialPageRoute(builder: (context) => const LoginScreen()));
                        },
                        icon: Icon(Icons.logout,color: screenColor,)
                      )
                    )
                  )
                )
              ],
            ),
            SizedBox(height: 10.0,),
            Padding(
              padding: EdgeInsets.only(left: 40.0, right: 40.0),
              child: Column(
                children: [
                  Container(
                    height: 300,
                    width: MediaQuery.of(context).size.width,
                    color: Color(0xFFC4C4C4),
                  ),
                  SizedBox(height: 30,),
                  Container(
                    padding: EdgeInsets.only(left: 12.0),
                    child: Text('Please scan or fill the receipt code',
                      style: TextStyle(fontSize: 17),
                    ),
                  ),
                  SizedBox(height: 10,),
                  TextField(
                    style: TextStyle(fontSize: 15),
                    decoration: InputDecoration(
                      hintText: "Please enter the receipt code",
                      fillColor: textFieldColor,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      isDense: true,                      
                      contentPadding: EdgeInsets.all(14),
                    ), 
                  ),
                  SizedBox(height: 15,),
                  SizedBox(
                    height: 45,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: primaryColor,
                        minimumSize: const Size.fromHeight(50), 
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        )
                      ),
                      onPressed: () => Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) => const FillInforScreen())),
                      child: Text('Next'),
                    )
                  )
                ]
              )
            )
          ],
        )
      )
    );
  }
}