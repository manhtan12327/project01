import 'package:flutter/material.dart';
import 'package:project1/constants.dart';
import 'package:project1/view/file_information_screen.dart';
import 'package:project1/view/finish_screen.dart';

class ReviewScreen extends StatefulWidget {
  const ReviewScreen({Key? key, required this.name, required this.company,
  required this.tax, required this.address, required this.phone, required this.fax, 
  required this.email, required this.bank,required this.account, required this.note, 
  required this.radio}) : super(key: key);

  final String name,company,tax,address,phone,fax,email,bank,account,note,radio;

  @override
  State<ReviewScreen> createState() => _ReviewScreenState();
}
enum account {personal,concern}
class _ReviewScreenState extends State<ReviewScreen> {
  account? _acc;
  @override
  Widget build(BuildContext context) {
    //check radio value
    if (widget.radio == 'account.personal'){
      _acc = account.personal;
    }else{
       _acc = account.concern;
    }
    //split text by comma
    String district = widget.address.split(',').first;
    String city = widget.address.split(',').last;
    return Scaffold(
      body: SafeArea(
        child: ListView(
          shrinkWrap: true,
          children: [
            Container(
              padding: EdgeInsets.only(top: 8.0,left: 5.0),
              child: Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: Container(
                      height: 40.0,
                      decoration: BoxDecoration(
                        color: reviewColor,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Center(
                        child: Text('Review',textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.bold,color: screenColor),
                        ),
                      )
                    )
                  ),
                  Expanded(
                    flex: 3,
                    child: Container() 
                  )
                ],
              )
            ),
            SizedBox(height: 20.0,),
            Center(
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 20.0, right: 20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text('Receipt code:  2010010199129010101',
                          style: TextStyle(fontWeight: FontWeight.bold), 
                        ),
                        SizedBox(height: 10,),
                        Text('Date: 30/07/2020 16:00:15',
                          style: TextStyle(fontWeight: FontWeight.bold), 
                        ),
                        SizedBox(height: 10,),
                        Text('Total:  230000 VND',
                          style: TextStyle(fontWeight: FontWeight.bold), 
                        ),
                        SizedBox(height: 10,),
                        Text('if this is not your receipt, please try again',
                          style: TextStyle(fontWeight: FontWeight.bold), 
                        ),
                        SizedBox(height: 10,),
                      ],
                    )
                  ),
                  Padding(
                      padding: EdgeInsets.only(left: 10.0, right: 10.0),
                      child: SizedBox(
                        height: 40,
                        child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: primaryColor,
                          minimumSize: const Size.fromHeight(50), 
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30),
                          )
                        ),
                        onPressed: () => Navigator.pop(context),
                        child: Text('Back'),
                      )
                    )
                  ),
                  Padding(
                    padding:EdgeInsets.only(left: 5.0, right: 5.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text('KH Cá Nhân', style: TextStyle(fontSize: 12)),
                                leading: Radio<account>(
                                  value: account.personal,
                                  groupValue: _acc,
                                  onChanged: (account? value) {
                                    setState(() {
                                      _acc = value;
                                    });
                                  },
                                )
                              )
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text('KH Doanh Nghiệp', style: TextStyle(fontSize: 12),),
                                leading: Radio<account>(
                                  value: account.concern,
                                  groupValue: _acc,
                                  onChanged: (account? value) {
                                    setState(() {
                                      _acc = value;
                                    });
                                  },
                                )
                              )
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  margin: EdgeInsets.only(right: 5.0),
                                  child: Text('Tên',style: TextStyle(fontSize: 12.0),),
                                )
                              )
                            ),
                            Expanded(
                              flex: 3,
                              child: TextField(
                                enabled: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  isDense: true,                      
                                  contentPadding: EdgeInsets.all(6),
                                  labelText: widget.name,
                                  labelStyle: TextStyle(fontWeight: FontWeight.bold)
                                ), 
                              )                          
                            )
                          ],
                        ),
                        SizedBox(height: 10.0,),
                        Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  margin: EdgeInsets.only(right: 5.0),
                                  child: Text('Tên công ty',style: TextStyle(fontSize: 12.0),),
                                )
                              )
                            ),
                            Expanded(
                              flex: 3,
                              child: TextField(
                                enabled: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  isDense: true,                      
                                  contentPadding: EdgeInsets.all(6),
                                  labelText: widget.company,
                                  labelStyle: TextStyle(fontWeight: FontWeight.bold)
                                ), 
                              )                          
                            )
                          ],
                        ),
                        SizedBox(height: 10.0,),
                        Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  margin: EdgeInsets.only(right: 5.0),
                                  child: Text('Mã số thuế',style: TextStyle(fontSize: 12.0),),
                                )
                              )
                            ),
                            Expanded(
                              flex: 3,
                              child: TextField(
                                enabled: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  isDense: true,                      
                                  contentPadding: EdgeInsets.all(6),
                                  labelText: widget.tax,
                                  labelStyle: TextStyle(fontWeight: FontWeight.bold)
                                ), 
                              )                          
                            )
                          ],
                        ),
                        SizedBox(height: 10.0,),
                        Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  margin: EdgeInsets.only(right: 5.0),
                                  child: Text('Địa chỉ',style: TextStyle(fontSize: 12.0),),
                                )
                              )
                            ),
                            Expanded(
                              flex: 3,
                              child: TextField(
                                enabled: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  isDense: true,                      
                                  contentPadding: EdgeInsets.all(6),
                                  labelText: widget.address,
                                  labelStyle: TextStyle(fontWeight: FontWeight.bold)
                                ), 
                              )                          
                            )
                          ],
                        ),
                        SizedBox(height: 10.0,),
                        Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  margin: EdgeInsets.only(right: 5.0),
                                  child: Text('Quận/Huyện',style: TextStyle(fontSize: 12.0),),
                                )
                              )
                            ),
                            Expanded(
                              flex: 3,
                              child: Row(
                                children: [
                                  Expanded( 
                                    flex: 1,
                                    child: TextField(
                                      enabled: false,
                                      decoration: InputDecoration(
                                        border: OutlineInputBorder(),
                                        isDense: true,                      
                                        contentPadding: EdgeInsets.all(6),
                                        labelText: district,
                                        labelStyle: TextStyle(fontWeight: FontWeight.bold)
                                      ), 
                                    ) 
                                  ),                         
                                  Expanded(
                                    flex: 1,
                                    child: Align(
                                      alignment: Alignment.centerRight,
                                      child: Container(
                                        margin: EdgeInsets.only(right: 5.0),
                                        child: Text('Tỉnh/Thành',style: TextStyle(fontSize: 12.0),),
                                      )
                                    )
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: TextField(
                                      enabled: false,
                                      decoration: InputDecoration(
                                        border: OutlineInputBorder(),
                                        isDense: true,                      
                                        contentPadding: EdgeInsets.all(6),
                                        labelText: city,
                                        labelStyle: TextStyle(fontWeight: FontWeight.bold)
                                      ), 
                                    )                          
                                  )
                                ]
                              )
                            )
                          ],
                        ),
                        SizedBox(height: 10.0,),
                        Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  margin: EdgeInsets.only(right: 5.0),
                                  child: Text('Điện thoại',style: TextStyle(fontSize: 12.0),),
                                )
                              )
                            ),
                            Expanded(
                              flex: 3,
                              child: Row(
                                children: [
                                  Expanded( 
                                    flex: 1,
                                    child: TextField(
                                      enabled: false,
                                      decoration: InputDecoration(
                                        border: OutlineInputBorder(),
                                        isDense: true,                      
                                        contentPadding: EdgeInsets.all(6),
                                        labelText: widget.phone,
                                        labelStyle: TextStyle(fontWeight: FontWeight.bold)
                                      ), 
                                    ) 
                                  ),                         
                                  Expanded(
                                    flex: 1,
                                    child: Align(
                                      alignment: Alignment.centerRight,
                                      child: Container(
                                        margin: EdgeInsets.only(right: 5.0),
                                        child: Text('Fax',style: TextStyle(fontSize: 12.0),),
                                      )
                                    )
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: TextField(
                                      enabled: false,
                                      decoration: InputDecoration(
                                        border: OutlineInputBorder(),
                                        isDense: true,                      
                                        contentPadding: EdgeInsets.all(6),
                                        labelText: widget.fax,
                                        labelStyle: TextStyle(fontWeight: FontWeight.bold)
                                      ), 
                                    )                          
                                  )
                                ]
                              )
                            )
                          ],
                        ),
                        SizedBox(height: 10.0,),
                        Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  margin: EdgeInsets.only(right: 5.0),
                                  child: Text('Email',style: TextStyle(fontSize: 12.0),),
                                )
                              )
                            ),
                            Expanded(
                              flex: 3,
                              child: TextField(
                                enabled: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  isDense: true,                      
                                  contentPadding: EdgeInsets.all(6),
                                  labelText: widget.email,
                                  labelStyle: TextStyle(fontWeight: FontWeight.bold)
                                ), 
                              )                          
                            )
                          ],
                        ),
                        SizedBox(height: 10.0,),
                        Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  margin: EdgeInsets.only(right: 5.0),
                                  child: Text('Tên ngân hàng',style: TextStyle(fontSize: 12.0),),
                                )
                              )
                            ),
                            Expanded(
                              flex: 3,
                              child: Row(
                                children: [
                                  Expanded( 
                                    flex: 1,
                                    child: TextField(
                                      enabled: false,
                                      decoration: InputDecoration(
                                        border: OutlineInputBorder(),
                                        isDense: true,                      
                                        contentPadding: EdgeInsets.all(6),
                                        labelText: widget.bank,
                                        labelStyle: TextStyle(fontWeight: FontWeight.bold)
                                      ), 
                                    ) 
                                  ),                         
                                  Expanded(
                                    flex: 1,
                                    child: Align(
                                      alignment: Alignment.centerRight,
                                      child: Container(
                                        margin: EdgeInsets.only(right: 5.0),
                                        child: Text('TK ngân hàng',style: TextStyle(fontSize: 12.0),),
                                      )
                                    )
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: TextField(
                                      enabled: false,
                                      decoration: InputDecoration(
                                        border: OutlineInputBorder(),
                                        isDense: true,                      
                                        contentPadding: EdgeInsets.all(6),
                                        labelText: widget.account,
                                        labelStyle: TextStyle(fontWeight: FontWeight.bold)
                                      ), 
                                    )                          
                                  )
                                ]
                              )
                            )
                          ],
                        ),
                        SizedBox(height: 10.0,),
                        Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  margin: EdgeInsets.only(right: 5.0),
                                  child: Text('Ghi chú',style: TextStyle(fontSize: 12.0),),
                                )
                              )
                            ),
                            Expanded(
                              flex: 3,
                              child: TextField(
                                enabled: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  isDense: true,                      
                                  contentPadding: EdgeInsets.all(6),
                                  labelText: widget.note,
                                  labelStyle: TextStyle(fontWeight: FontWeight.bold)
                                ), 
                              )                          
                            )
                          ],
                        ),
                        SizedBox(height: 20.0,),
                      ],
                    )
                  ),
                  Padding(
                      padding: EdgeInsets.only(left: 10.0, right: 10.0),
                      child: SizedBox(
                        height: 40,
                        child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: primaryColor,
                          minimumSize: const Size.fromHeight(50), 
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30),
                          )
                        ),
                        onPressed: () => Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) => const FinishScreen())),
                        child: Text('Next'),
                      )
                    )
                  ),
                ]
              )
            )
          ]
        )
      )
    );
  }
}