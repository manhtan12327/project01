import 'package:flutter/material.dart';
import 'package:project1/constants.dart';
import 'package:project1/view/login_screen.dart';

class SetServerScreen extends StatefulWidget {
  const SetServerScreen({Key? key}) : super(key: key);

  @override
  State<SetServerScreen> createState() => _SetServerScreenState();
}

class _SetServerScreenState extends State<SetServerScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: screenColor,
      body: SafeArea(     
        child: Column(
          children: [ 
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(left: 40.0, right: 40.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 12.0),
                      child: Text('Server URL',
                        style: TextStyle(fontSize: 17),
                      ),
                    ),
                    SizedBox(height: 10,),
                    TextField(
                      style: TextStyle(fontSize: 15),
                      decoration: InputDecoration(
                        hintText: "Server URL",
                        fillColor: textFieldColor,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        isDense: true,                      
                        contentPadding: EdgeInsets.all(14),
                      ), 
                    ),
                    SizedBox(height: 20,),
                    Container(
                      padding: EdgeInsets.only(left: 12.0),
                      child: Text('Port',
                        style: TextStyle(fontSize: 17),
                      ),
                    ),
                    SizedBox(height: 10,),
                    TextField(
                      style: TextStyle(fontSize: 15),
                      decoration: InputDecoration(
                        hintText: "Port",
                        fillColor: textFieldColor,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        isDense: true,                      
                        contentPadding: EdgeInsets.all(14),
                      ), 
                    ),
                    SizedBox(height: 20,),
                    SizedBox(
                      height: 45,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: primaryColor,
                          minimumSize: const Size.fromHeight(50), 
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30),
                          )
                        ),
                        onPressed: () => Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) => const LoginScreen())),
                        child: Text('Save'),
                      )
                    )
                  ] 
                )
              )
            )
          ]
        )
      )
    );
  }
}