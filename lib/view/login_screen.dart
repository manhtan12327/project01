import 'package:flutter/material.dart';
import 'package:project1/constants.dart';
import 'package:project1/view/home_screen.dart';
import 'package:project1/view/set_server_screen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: screenColor,
      body: SafeArea(     
        child: Column(
          children: [ 
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  padding: EdgeInsets.all(5.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10.0),
                    child: Container(
                      color: primaryColor,
                      child: IconButton(
                        onPressed:() {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return Dialog(
                                backgroundColor: dialogColor,
                                child: Container(
                                  height: 200,
                                  child: Padding(
                                    padding: EdgeInsets.only(left: 10.0, right: 10.0),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text('Password',style: TextStyle(fontWeight: FontWeight.bold),),
                                        SizedBox(height: 10.0,),
                                        TextField(
                                          style: TextStyle(fontSize: 15),
                                          decoration: InputDecoration(
                                            hintText: "password",
                                            fillColor: textFieldColor,
                                            border: OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(20),
                                            ),
                                            isDense: true,                      
                                            contentPadding: EdgeInsets.all(14),
                                          ), 
                                        ),
                                        SizedBox(height: 10,),
                                        SizedBox(
                                          height: 45,
                                          child: ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              primary: primaryColor,
                                              minimumSize: const Size.fromHeight(50), 
                                              shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(30),
                                              )
                                            ),
                                            onPressed: () => Navigator.of(context)
                                              .push(MaterialPageRoute(builder: (context) => const SetServerScreen())),
                                            child: Text('Save'),
                                          )
                                        )
                                      ]
                                    )
                                  )
                                ),
                              );
                            }
                          );                    
                        },
                        icon: Icon(Icons.settings,color: screenColor,)
                      )
                    )
                  )
                )
              ],
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(left: 40.0, right: 40.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 12.0),
                      child: Text('Username',
                        style: TextStyle(fontSize: 17),
                      ),
                    ),
                    SizedBox(height: 10,),
                    TextField(
                      style: TextStyle(fontSize: 15),
                      decoration: InputDecoration(
                        hintText: "username",
                        fillColor: textFieldColor,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        isDense: true,                      
                        contentPadding: EdgeInsets.all(14),
                      ), 
                    ),
                    SizedBox(height: 20,),
                    Container(
                      padding: EdgeInsets.only(left: 12.0),
                      child: Text('Password',
                        style: TextStyle(fontSize: 17),
                      ),
                    ),
                    SizedBox(height: 10,),
                    TextField(
                      style: TextStyle(fontSize: 15),
                      decoration: InputDecoration(
                        hintText: "password",
                        fillColor: textFieldColor,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        isDense: true,                      
                        contentPadding: EdgeInsets.all(14),
                      ), 
                    ),
                    SizedBox(height: 20,),
                    SizedBox(
                      height: 45,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: primaryColor,
                          minimumSize: const Size.fromHeight(50), 
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30),
                          )
                        ),
                        onPressed: () => Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) => const HomeScreen())),
                        child: Text('Login'),
                      )
                    )
                  ] 
                )
              )
            )
          ]
        )
      )
    );
  }
}