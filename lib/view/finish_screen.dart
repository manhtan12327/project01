import 'package:flutter/material.dart';
import 'package:project1/constants.dart';
import 'package:project1/view/home_screen.dart';

class FinishScreen extends StatelessWidget {
  const FinishScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
          children:[   
            Center( 
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(
                  color: primaryColor,width: 3.0 
                ),
                borderRadius: BorderRadius.all(Radius.circular(30))
              ),
                child: Icon(
                  Icons.done,color: primaryColor,size: 60.0,
                )
              )
            ),
            SizedBox(height: 10.0,),
            Center(
              child: Text('Your information is updated',style: TextStyle(fontWeight: FontWeight.bold),),
            )
          ] 
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.only(right: 20,left: 20,bottom: 20),
        child:SizedBox(
          height: 40.0,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: primaryColor,
              minimumSize: const Size.fromHeight(50), 
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
              )
            ),
          onPressed: () => Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => const HomeScreen())),
            child: Text('Back'),
          )
        )
      )
    );
  }
}