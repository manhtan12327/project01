import 'package:flutter/material.dart';
import 'package:project1/constants.dart';
import 'package:project1/view/review_screen.dart';

class FillInforScreen extends StatefulWidget {
  const FillInforScreen({Key? key}) : super(key: key);

  @override
  State<FillInforScreen> createState() => _FillInforScreenState();
}

enum account {personal,concern}

class _FillInforScreenState extends State<FillInforScreen> {
  account? _acc = account.personal;
  TextEditingController nameController = TextEditingController();
  TextEditingController companyController = TextEditingController();
  TextEditingController taxController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController faxController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController bankController = TextEditingController();
  TextEditingController accountController = TextEditingController();
  TextEditingController noteController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ListView(
          shrinkWrap: true,
          children: [
            Container(
              padding: EdgeInsets.only(top: 8.0,left: 5.0),
              child: Row(
                children: [
                  Column(
                    children: [
                      Container(
                        height: 30,
                        width: 50,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: NetworkImage(
                              'https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Flag_of_Vietnam.svg/2000px-Flag_of_Vietnam.svg.png'),
                              fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      SizedBox(height: 5.0,),
                      Center(
                        child: Text('Tiếng Việt',
                          style: TextStyle(fontSize: 12.0),
                        ),
                      )
                    ]
                  ),
                  SizedBox(width: 10.0,),
                  Column(
                    children: [
                      Container(
                        height: 30,
                        width: 50,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: NetworkImage(
                              'https://upload.wikimedia.org/wikipedia/en/thumb/a/a4/Flag_of_the_United_States.svg/1200px-Flag_of_the_United_States.svg.png'),
                              fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      SizedBox(height: 5.0,),
                      Center(
                        child: Text('English',
                          style: TextStyle(fontSize: 12.0),
                        ),
                      )
                    ]
                  )
                ],
              )
            ),
            SizedBox(height: 20.0,),
            Center(
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 20.0, right: 20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text('Mã đơn hàng:  2010010199129010101',
                          style: TextStyle(fontWeight: FontWeight.bold), 
                        ),
                        SizedBox(height: 10,),
                        Text('Ngày: 30/07/2020 16:00:15',
                          style: TextStyle(fontWeight: FontWeight.bold), 
                        ),
                        SizedBox(height: 10,),
                        Text('Tổng:  230000 VND',
                          style: TextStyle(fontWeight: FontWeight.bold), 
                        ),
                        SizedBox(height: 10,),
                        Text('Nếu không phải đơn hàng của bạn, vui lòng thử lại',
                          style: TextStyle(fontWeight: FontWeight.bold), 
                        ),
                        SizedBox(height: 10,),
                      ],
                    )
                  ),
                  Padding(
                      padding: EdgeInsets.only(left: 10.0, right: 10.0),
                      child: SizedBox(
                        height: 40,
                        child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: primaryColor,
                          minimumSize: const Size.fromHeight(50), 
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30),
                          )
                        ),
                        onPressed: () => Navigator.pop(context),
                        child: Text('Trở lại'),
                      )
                    )
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 5.0, right: 5.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                title: const Text('KH Cá Nhân', style: TextStyle(fontSize: 12)),
                                leading: Radio<account>(
                                  value: account.personal,
                                  groupValue: _acc,
                                  onChanged: (account? value) {
                                    setState(() {
                                      _acc = value;
                                    });
                                  },
                                )
                              )
                            ),
                            Expanded(
                              child: ListTile(
                                title: const Text('KH Doanh Nghiệp', style: TextStyle(fontSize: 12),),
                                leading: Radio<account>(
                                  value: account.concern,
                                  groupValue: _acc,
                                  onChanged: (account? value) {
                                    setState(() {
                                      _acc = value;
                                    });
                                  },
                                )
                              )
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  margin: EdgeInsets.only(right: 5.0),
                                  child: Text('Tên',style: TextStyle(fontSize: 12.0),),
                                )
                              )
                            ),
                            Expanded(
                              flex: 3,
                              child: TextField(
                                controller: nameController,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  isDense: true,                      
                                  contentPadding: EdgeInsets.all(6),
                                ), 
                              )                          
                            )
                          ],
                        ),
                        SizedBox(height: 10.0,),
                        Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  margin: EdgeInsets.only(right: 5.0),
                                  child: Text('Tên công ty',style: TextStyle(fontSize: 12.0),),
                                )
                              )
                            ),
                            Expanded(
                              flex: 3,
                              child: TextField(
                                controller: companyController,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  isDense: true,                      
                                  contentPadding: EdgeInsets.all(6),
                                ), 
                              )                          
                            )
                          ],
                        ),
                        SizedBox(height: 10.0,),
                        Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  margin: EdgeInsets.only(right: 5.0),
                                  child: Text('Mã số thuế',style: TextStyle(fontSize: 12.0),),
                                )
                              )
                            ),
                            Expanded(
                              flex: 3,
                              child: TextField(
                                controller: taxController,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  isDense: true,                      
                                  contentPadding: EdgeInsets.all(6),
                                ), 
                              )                          
                            )
                          ],
                        ),
                        SizedBox(height: 10.0,),
                        Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  margin: EdgeInsets.only(right: 5.0),
                                  child: Text('Địa chỉ',style: TextStyle(fontSize: 12.0),),
                                )
                              )
                            ),
                            Expanded(
                              flex: 3,
                              child: TextField(
                                controller: addressController,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  isDense: true,                      
                                  contentPadding: EdgeInsets.all(6),
                                ), 
                              )                          
                            )
                          ],
                        ),
                        SizedBox(height: 10.0,),
                        Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  margin: EdgeInsets.only(right: 5.0),
                                  child: Text('Điện thoại',style: TextStyle(fontSize: 12.0),),
                                )
                              )
                            ),
                            Expanded(
                              flex: 3,
                              child: Row(
                                children: [
                                  Expanded( 
                                    flex: 1,
                                    child: TextField(
                                      controller: phoneController,
                                      decoration: InputDecoration(
                                        border: OutlineInputBorder(),
                                        isDense: true,                      
                                        contentPadding: EdgeInsets.all(6),
                                      ), 
                                    ) 
                                  ),                         
                                  Expanded(
                                    flex: 1,
                                    child: Align(
                                      alignment: Alignment.centerRight,
                                      child: Container(
                                        margin: EdgeInsets.only(right: 5.0),
                                        child: Text('Fax',style: TextStyle(fontSize: 12.0),),
                                      )
                                    )
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: TextField(
                                      controller: faxController,
                                      decoration: InputDecoration(
                                        border: OutlineInputBorder(),
                                        isDense: true,                      
                                        contentPadding: EdgeInsets.all(6),
                                      ), 
                                    )                          
                                  )
                                ]
                              )
                            )
                          ],
                        ),
                        SizedBox(height: 10.0,),
                        Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  margin: EdgeInsets.only(right: 5.0),
                                  child: Text('Email',style: TextStyle(fontSize: 12.0),),
                                )
                              )
                            ),
                            Expanded(
                              flex: 3,
                              child: TextField(
                                controller: emailController,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  isDense: true,                      
                                  contentPadding: EdgeInsets.all(6),
                                ), 
                              )                          
                            )
                          ],
                        ),
                        SizedBox(height: 10.0,),
                        Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  margin: EdgeInsets.only(right: 5.0),
                                  child: Text('Tên ngân hàng ',style: TextStyle(fontSize: 12.0),),
                                )
                              )
                            ),
                            Expanded(
                              flex: 3,
                              child: Row(
                                children: [
                                  Expanded( 
                                    flex: 1,
                                    child: TextField(
                                      controller: bankController,
                                      decoration: InputDecoration(
                                        border: OutlineInputBorder(),
                                        isDense: true,                      
                                        contentPadding: EdgeInsets.all(6),
                                      ), 
                                    ) 
                                  ),                         
                                  Expanded(
                                    flex: 1,
                                    child: Align(
                                      alignment: Alignment.centerRight,
                                      child: Container(
                                        margin: EdgeInsets.only(right: 5.0),
                                        child: Text('TK ngân hàng ',style: TextStyle(fontSize: 12.0),),
                                      )
                                    )
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: TextField(
                                      controller: accountController,
                                      decoration: InputDecoration(
                                        border: OutlineInputBorder(),
                                        isDense: true,                      
                                        contentPadding: EdgeInsets.all(6),
                                      ), 
                                    )                          
                                  )
                                ]
                              )
                            )
                          ],
                        ),
                        SizedBox(height: 10.0,),
                        Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Container(
                                  margin: EdgeInsets.only(right: 5.0),
                                  child: Text('Ghi chú',style: TextStyle(fontSize: 12.0),),
                                )
                              )
                            ),
                            Expanded(
                              flex: 3,
                              child: TextField(
                                controller: noteController,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  isDense: true,                      
                                  contentPadding: EdgeInsets.all(6),
                                ), 
                              )                          
                            )
                          ],
                        ),
                        SizedBox(height: 20.0,),
                      ],
                    )
                  ),
                  Padding(
                      padding: EdgeInsets.only(left: 10.0, right: 10.0),
                      child: SizedBox(
                        height: 40,
                        child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: primaryColor,
                          minimumSize: const Size.fromHeight(50), 
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30),
                          )
                        ),
                        onPressed: () => Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) => ReviewScreen(name: nameController.text,
                          company: companyController.text,tax: taxController.text,address: addressController.text,
                          phone: phoneController.text,fax: faxController.text,email: emailController.text,
                          bank: bankController.text,account: accountController.text,
                          note: noteController.text,radio: _acc.toString(),))),
                        child: Text('Tiếp tục'),
                      )
                    )
                  ),
                ]
              )
            )
          ]
        )
      )
    );
  }
}