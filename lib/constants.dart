import 'package:flutter/material.dart';

const Color primaryColor = Color(0xFFF60000);

const Color screenColor = Colors.white;

const Color textFieldColor = Color(0xFFF8F8F8);

const Color reviewColor = Color(0xFFF2994A);

const Color dialogColor = Color(0xFFD9D9D9);